const HOOK_COMMISSION_SALE_PRICE = '[js-hook-commission-calculator-sale-price]'
const HOOK_COMMISSION_COMMISSION_FEE = '[js-hook-commission-calculator-commission-fee]'
const HOOK_COMMISSION_REVENUE = '[js-hook-commission-calculator-revenue]'
const HOOK_COMMISSION_CONTACT = '[js-hook-commission-contact]'

const CLASS_HIDDEN = 'u-hide'

class CommissionCalculator {
  private salePriceInput: HTMLInputElement
  private commissionFeeField: HTMLElement
  private commissionFee: number
  private commissionFeePercent: number
  private commissionFeeInput: HTMLInputElement
  private revenueField: HTMLElement
  private revenueInput: HTMLInputElement
  private contact: HTMLElement

  constructor(element: HTMLElement) {
    this.salePriceInput = element
      .querySelector<HTMLElement>(HOOK_COMMISSION_SALE_PRICE)!
      .querySelector<HTMLInputElement>('input')!
    this.commissionFeeField = element.querySelector<HTMLElement>(HOOK_COMMISSION_COMMISSION_FEE)!
    this.commissionFeeInput = element
      .querySelector<HTMLElement>(HOOK_COMMISSION_COMMISSION_FEE)!
      .querySelector<HTMLInputElement>('input')!
    this.revenueField = element.querySelector<HTMLElement>(HOOK_COMMISSION_REVENUE)!
    this.revenueInput = element
      .querySelector<HTMLElement>(HOOK_COMMISSION_REVENUE)!
      .querySelector<HTMLInputElement>('input')!

    this.commissionFeePercent = parseInt(element.dataset.percent as string) || 12.5
    this.contact = element.querySelector(HOOK_COMMISSION_CONTACT)!

    this.bindEvents()
  }

  private bindEvents() {
    this.salePriceInput.addEventListener('keyup', (e: Event) => {
      const salePrice = parseInt((e.currentTarget as HTMLInputElement).value)

      if (salePrice >= 10000) {
        this.commissionFeeField.classList.add(CLASS_HIDDEN)
        this.revenueField.classList.add(CLASS_HIDDEN)
        this.contact.classList.remove(CLASS_HIDDEN)
      } else {
        this.commissionFeeField.classList.remove(CLASS_HIDDEN)
        this.revenueField.classList.remove(CLASS_HIDDEN)
        this.contact.classList.add(CLASS_HIDDEN)
        this.commissionFee = isNaN(salePrice) ? 0 : (salePrice / 100) * this.commissionFeePercent

        this.commissionFeeInput.value = isNaN(salePrice) ? '' : this.commissionFee + ''
        this.revenueInput.value = isNaN(salePrice) ? '' : salePrice - this.commissionFee + ''
      }
    })
  }
}

export default CommissionCalculator
