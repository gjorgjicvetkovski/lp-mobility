/**
 * @shelf-version: 1.1.0
 */

import ScrollTo from './javascript/scroll-to'

export default ScrollTo
