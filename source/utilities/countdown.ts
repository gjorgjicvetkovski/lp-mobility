class Countdown {
  private element: HTMLElement
  private endDate: string

  constructor(element: HTMLElement) {
    this.element = element
    this.endDate = `${element.dataset.endDate}.000+02:00`
    this.bindEvents()
  }

  private bindEvents() {
    // Set the date we're counting down to
    // const countDownDate = new Date("Jan 5, 2022 15:37:25").getTime();
    const countDownDate = new Date(this.endDate).getTime();

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this

    // Update the count down every 1 second
    const x = setInterval(function() {

      // Get today's date and time
      const now = new Date().getTime();

      // Find the distance between now and the count down date
      const distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
      if (days >= 1) {
        self.element.innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
      } else {
        self.element.innerHTML = hours + "h " + minutes + "m " + seconds + "s ";
      }

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        self.element.innerHTML = "EXPIRED";
      }
    }, 1000);
  }
}

export default Countdown
