'use strict'

/*------------------------------------*\
 * JS Main entry file
 \*------------------------------------*/
// import './config'
// import '@utilities/detect-touch'
// import '@utilities/detect-keyboard-focus'
//
// import moduleInit from '@utilities/module-init'

// import Example from '@components/example' // Sync
// moduleInit.sync('[js-hook-module-example]', Example) // Sync

// moduleInit.async('[js-hook-module-example]', () => import('@components/example')) // Async
// moduleInit.async('[js-hook-carousel]', () =>
//   import(/* webpackChunkName: "Carousel" */ '@components/carousel'),
// )
// moduleInit.async('[js-hook-countdown]', () =>
//   import(/* webpackChunkName: "Countdown" */ '@utilities/countdown'),
// )
// moduleInit.async('[js-hook-accordion]', () =>
//   import(/* webpackChunkName: "Accordion" */ '@components/accordion'),
// )
// moduleInit.async('[js-hook-commission-calculator]', () =>
//   import(/* webpackChunkName: "Commission Calculator" */ '@components/commission-calculator'),
// )

import Accordion from '@components/accordion'
import Carousel from '@components/carousel'
import CommissionCalculator from '@components/commission-calculator'
import Countdown from '@utilities/countdown'

const carousels = [...document.querySelectorAll('[js-hook-carousel]')]
const countdowns = [...document.querySelectorAll('[js-hook-countdown]')]
const accordions = [...document.querySelectorAll('[js-hook-accordion]')]
const commissionCalculators = [...document.querySelectorAll('[js-hook-commission-calculator]')]

carousels.forEach(carousel => new Carousel(carousel))
countdowns.forEach(countdown => new Countdown(countdown))
accordions.forEach(accordion => new Accordion(accordion))
commissionCalculators.forEach(calculator => new CommissionCalculator(calculator))
